# BOOK RENTALS

This repository contains a simple Python script(`core.py`) which contains the logic for calculating the charges on book rentals.

## Usage

Clone the repo:

```bash
git clone https://gitlab.com/erickmbwana/bookrentals.git
```

Make sure you are within the repository and using Python 2.7 or Python >=3.5. You can run :

```bash
python core.py
```

You can change the rent calculating function called in the `__if__ main` block in the core.py script. The inputs can also updated to test various scenarios.

## Tests

You can run the tests thus:

```bash
python tests.py
```
