'''
This module contains logic for calculating charges on book rentals.
'''

BOOK_CHARGE_PER_DAY = 1

REGULAR_BOOK_CHARGE_PER_DAY = 1.5
FICTION_BOOK_CHARGE_PER_DAY = 3
NOVEL_BOOK_CHARGE_PER_DAY = 1.5

BOOK_CHARGES = {
    'regular': REGULAR_BOOK_CHARGE_PER_DAY,
    'fiction': FICTION_BOOK_CHARGE_PER_DAY,
    'novel': NOVEL_BOOK_CHARGE_PER_DAY
}


def calculate_book_rent1(num_books, num_days):
    '''
    Story 1 solution

    Assume all books are rented for the same no of days.
    '''
    rent = num_books * num_days * BOOK_CHARGE_PER_DAY
    return rent


def _get_charge(book_type):
    '''
    Helper function to validate book type and get its associated charge.
    '''
    book_types = BOOK_CHARGES.keys()

    error = 'Book type should be either regular, fiction or novel'
    assert book_type in book_types, error

    charge = BOOK_CHARGES[book_type]
    return charge


def calculate_book_rent2(book_type, num_books, num_days):
    '''
    Story 2 solution
    '''
    charge = _get_charge(book_type)
    rent = num_books * num_days * charge
    return rent


def calculate_book_rent3(book_type, num_books, num_days):
    '''
    Story 3 solution
    '''
    standard_charge = _get_charge(book_type)

    if book_type == 'novel':
        rent = num_books * num_days * standard_charge
        rent = rent if num_days >= 3 else 4.5
    elif book_type == 'regular':
        first_two_days_charge = 2 * num_books
        rem_days_charge = (num_days - 2) * standard_charge * num_books
        rent = first_two_days_charge + rem_days_charge
        rent = rent if num_days >= 2 else 2
    else:
        rent = num_books * num_days * standard_charge
    return rent


if __name__ == '__main__':
    rent = calculate_book_rent3('regular', 10, 5)
    print(rent)