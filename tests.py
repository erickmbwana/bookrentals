import unittest

from core import (
    calculate_book_rent1, calculate_book_rent2, calculate_book_rent3
)


class RentCalculationTestCase(unittest.TestCase):

    def test_solution1(self):
        actual = calculate_book_rent1(10, 5)
        expected = 50

        self.assertEqual(actual, expected)

    def test_solution2_regular(self):
        actual = calculate_book_rent2('regular', 10, 2)
        expected = 30

        self.assertEqual(actual, expected)
    
    def test_solution2_fiction(self):
        actual = calculate_book_rent2('fiction', 10, 2)
        expected = 60

        self.assertEqual(actual, expected)
    
    def test_solution2_novel(self):
        actual = calculate_book_rent2('novel', 10, 2)
        expected = 30

        self.assertEqual(actual, expected)

    def test_solution2_wrong_type(self):
        with self.assertRaises(AssertionError):
            calculate_book_rent2('wrong', 10, 2)

    def test_solution3_regular(self):
        actual = calculate_book_rent3('regular', 10, 5)
        expected = 65

        self.assertEqual(actual, expected)
    
    def test_solution3_regular_few_days(self):
        actual = calculate_book_rent3('regular', 10, 1)
        expected = 2

        self.assertEqual(actual, expected)
    
    def test_solution3_fiction(self):
        actual = calculate_book_rent3('fiction', 10, 5)
        expected = 150
        self.assertEqual(actual, expected)
    
    def test_solution3_novel(self):
        actual = calculate_book_rent3('novel', 10, 5)
        expected = 75

        self.assertEqual(actual, expected)
    
    def test_solution3_novel_few_days(self):
        actual = calculate_book_rent3('novel', 10, 2)
        expected = 4.5

        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
